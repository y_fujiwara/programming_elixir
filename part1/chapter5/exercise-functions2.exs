fizzBuzz = fn 
  0, 0, _ -> "FizzBazz"
  0, _, _ -> "Fizz"
  _, 0, _ -> "Bazz"
  _, _, c -> c
end

IO.inspect fizzBuzz.(1,2,3)
IO.inspect fizzBuzz.(0,2,3)
IO.inspect fizzBuzz.(1,0,3)
IO.inspect fizzBuzz.(0,0,3)
