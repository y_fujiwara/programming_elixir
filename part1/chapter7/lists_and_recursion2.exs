defmodule MyList do
  def max([head]) do
    head
  end

  def max([head | [ tail_head | tail_tail]] ) when head > tail_head do
    max([head | tail_tail])
  end
  
  def max([head | [ tail_head | tail_tail]] ) when head < tail_head do
    max([tail_head | tail_tail])
  end
end
