defmodule Exercise2 do
  # ユークリッドの互除法
  def gcd(x, 0), do: x
  def gcd(x, y), do: gcd(y, rem(x, y))
end
