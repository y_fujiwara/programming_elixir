defmodule FibSolver do

  def fib(scheduler) do
    # schedulerに:readyメッセージを出して待機させる
    send scheduler, { :ready, self }
    receive do
      { :fib, n, client } ->
        send client, { :answer, n, fib_calc(n), self }
        fib(scheduler) # これによってまたすぐreadyメッセージが送られて待機状態に入る
      { :shutdown } ->
        exit(:normal)
    end
  end

  # very inefficient, deliberately
  defp fib_calc(0), do: 0
  defp fib_calc(1), do: 1
  defp fib_calc(n), do: fib_calc(n-1) + fib_calc(n-2)
end

defmodule Scheduler do
  
  def run(num_processes, module, func, to_calculate) do
    (1..num_processes)
    |> Enum.map(fn(_) -> spawn(module, func, [self]) end)
    |> schedule_processes(to_calculate, [])
  end

  defp schedule_processes(processes, queue, results) do
    receive do
      {:ready, pid} when length(queue) > 0 ->
        IO.puts "call :ready #{:erlang.pid_to_list(pid)} #{inspect processes}"
        
        [ next | tail ] = queue
        send pid, {:fib, next, self}
        # sendの結果を待たない(非同期)すぐに次の待受にはいるが、runの時点でsendされているので、それが処理されていく
        # processesの数分再帰されて初めてちゃんとした待受状態にはいる
        schedule_processes(processes, tail, results)

      {:ready, pid} ->
        # queueよりprocessesが多い場合は、余ったPIDに対するsendはすぐここに入ってしまう
        send pid, {:shutdown}
        if length(processes) > 1 do
          # 計算し終わったプロセスを削除する
          IO.puts "call :ready finish #{:erlang.pid_to_list(pid)} #{inspect results}"
          schedule_processes(List.delete(processes, pid), queue, results)
        else
          Enum.sort(results, fn {n1, _}, {n2, _} -> n1 <= n2 end)
        end

      {:answer, number, result, pid} ->
        IO.puts "call :answer #{:erlang.pid_to_list(pid)} #{result}"
        # 再帰するが、重い処理でリストのサイズがqueue < processesの場合は基本的にはqueueは0になるはずなので、二個目のreadyにはいる
        schedule_processes(processes, queue, [ {number, result} | results])
    end
  end
end

to_process = [ 37, 37, 37 ]

Enum.each 1..6, fn num_processes ->
  {time, result} = :timer.tc(
    Scheduler, :run,
    [num_processes, FibSolver, :fib, to_process]
  )

  if num_processes == 1 do
    IO.puts inspect result
    IO.puts "\n #   time(s)"
  end
  :io.format "~2B     ~.2f~n", [num_processes, time/1000000.0]
end
