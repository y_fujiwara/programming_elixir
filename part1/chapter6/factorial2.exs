defmodule Factorial2 do
  def of(0), do: 1

  # 負の値が入っても大丈夫なように
  def of(n) when n > 0 do
    n * of(n-1)
  end
end
