fizzbuzzDummy = fn 
  0, 0, _ -> "FizzBazz"
  0, _, _ -> "Fizz"
  _, 0, _ -> "Bazz"
  _, _, c -> c
end

fizzBuzz = fn a -> fizzbuzzDummy.(rem(a, 3), rem(a, 5), a) end

IO.puts fizzBuzz.(10)
IO.puts fizzBuzz.(11)
IO.puts fizzBuzz.(12)
IO.puts fizzBuzz.(13)
IO.puts fizzBuzz.(14)
IO.puts fizzBuzz.(15)
IO.puts fizzBuzz.(16)
IO.puts fizzBuzz.(17)
