defmodule MyList do
  def mapsum([head], func) do
    func.(head)
  end

  def mapsum([head | tail], func) do
    func.(head) + mapsum(tail, func)
  end
end
