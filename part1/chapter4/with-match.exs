result = with {:ok, file} = File.open("./etc/passwd"),
              content     = IO.read(file, :all),
              :ok         = File.close(file),
              [_, uid, gid] <- Regex.run(~r/xxx:.*?:(\d+):(\d+)/, content)
         do
              "Group: #{uid}, User: #{gid}"
         end
IO.puts inspect(result)
                

