defmodule Chop do
  # def guess(n, min..max \\ 1..1000)

  def mid(n) do
    mid = div(n, 2)
    IO.puts "Is it #{mid}"
    mid
  end

  def guess(n, min..max) when n == div(min + max, 2) do
    mid(min + max)
  end

  def guess(n, min..max) when n < div(min + max, 2) do
    guess(n, min..mid(min + max))
  end

  def guess(n, min..max) when n > div(min + max, 2) do
    guess(n, mid(min + max)..max)
  end
end
