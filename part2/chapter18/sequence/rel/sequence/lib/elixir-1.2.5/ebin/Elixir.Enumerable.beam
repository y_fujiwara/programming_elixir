FOR1  *�BEAMExDc  �hd elixir_docs_v1l   hd docsl   hhd __protocol__aad defl   hd atomjd Elixirjd falsehhd countaa�d defl   hd 
enumerablejd niljm  IRetrieves the enumerable's size.

It should return `{:ok, size}`.

If `{:error, __MODULE__}` is returned a default algorithm using
`reduce` and the match (`===`) operator is used. This algorithm runs
in linear time.

_Please force use of the default algorithm unless you can implement an
algorithm that is significantly faster._
hhd impl_foraad defl   hd datajd niljd nilhhd 	impl_for!aad defl   hd datajd niljd nilhhd member?aavd defl   hd 
enumerablejd nilhd elementjd niljm  ^Checks if an element exists within the enumerable.

It should return `{:ok, boolean}`.

If `{:error, __MODULE__}` is returned a default algorithm using
`reduce` and the match (`===`) operator is used. This algorithm runs
in linear time.

_Please force use of the default algorithm unless you can implement an
algorithm that is significantly faster._
hhd reduceaacd defl   hd 
enumerablejd nilhd accjd nilhd funjd niljm  iReduces the enumerable into an element.

Most of the operations in `Enum` are implemented in terms of reduce.
This function should apply the given `t:reducer/0` function to each
item in the enumerable and proceed as expected by the returned
accumulator.

As an example, here is the implementation of `reduce` for lists:

    def reduce(_,     {:halt, acc}, _fun),   do: {:halted, acc}
    def reduce(list,  {:suspend, acc}, fun), do: {:suspended, acc, &reduce(list, &1, fun)}
    def reduce([],    {:cont, acc}, _fun),   do: {:done, acc}
    def reduce([h|t], {:cont, acc}, fun),    do: reduce(t, fun.(h, acc), fun)

jhd 	moduledocham  2Enumerable protocol used by `Enum` and `Stream` modules.

When you invoke a function in the `Enum` module, the first argument
is usually a collection that must implement this protocol.
For example, the expression:

    Enum.map([1, 2, 3], &(&1 * 2))

invokes `Enumerable.reduce/3` to perform the reducing
operation that builds a mapped list by calling the mapping function
`&(&1 * 2)` on every element in the collection and consuming the
element with an accumulated list.

Internally, `Enum.map/2` is implemented as follows:

    def map(enum, fun) do
      reducer = fn x, acc -> {:cont, [fun.(x)|acc]} end
      Enumerable.reduce(enum, {:cont, []}, reducer) |> elem(1) |> :lists.reverse()
    end

Notice the user-supplied function is wrapped into a `t:reducer/0` function.
The `t:reducer/0` function must return a tagged tuple after each step,
as described in the `t:acc/0` type.

The reason the accumulator requires a tagged tuple is to allow the
`t:reducer/0` function to communicate the end of enumeration to the underlying
enumerable, allowing any open resources to be properly closed.
It also allows suspension of the enumeration, which is useful when
interleaving between many enumerables is required (as in zip).

Finally, `Enumerable.reduce/3` will return another tagged tuple,
as represented by the `t:result/0` type.
hd callback_docsl   hhd countaa�d callbackd nilhhd member?aa�d callbackd nilhhd reduceaasd callbackd niljhd 	type_docsl   hhd acca a%d typem  4The accumulator value for each step.

It must be a tagged tuple with one of the following "tags":

  * `:cont`    - the enumeration should continue
  * `:halt`    - the enumeration should halt immediately
  * `:suspend` - the enumeration should be suspended immediately

Depending on the accumulator value, the result returned by
`Enumerable.reduce/3` will change. Please check the `t:result/0`
type documentation for more information.

In case a `t:reducer/0` function returns a `:suspend` accumulator,
it must be explicitly handled by the caller and never leak.
hhd continuationa aTd typem  �A partially applied reduce function.

The continuation is the closure returned as a result when
the enumeration is suspended. When invoked, it expects
a new accumulator and it returns the result.

A continuation is easily implemented as long as the reduce
function is defined in a tail recursive fashion. If the function
is tail recursive, all the state is passed as arguments, so
the continuation would simply be the reducing function partially
applied.
hhd reducera a7d typem   �The reducer function.

Should be called with the enumerable element and the
accumulator contents.

Returns the accumulator for the next enumeration step.
hhd resulta aAd typem  TThe result of the reduce operation.

It may be *done* when the enumeration is finished by reaching
its end, or *halted*/*suspended* when the enumeration was halted
or suspended by the `t:reducer/0` function.

In case a `t:reducer/0` function returns the `:suspend` accumulator, the
`:suspended` tuple must be explicitly handled by the caller and
never leak. In practice, this means regular enumeration functions
just need to be concerned about `:done` and `:halted` results.

Furthermore, a `:suspend` call must always be followed by another call,
eventually halting or continuing until the end.
hhd ta ad typed niljjAtom  �   /Elixir.Enumerable__info__	functionsmacroserlangget_module_inforeduce__protocol__consolidated?modulefalsecountmember?	impl_for!nilvalueElixir.Protocol.UndefinedError	exceptionerror	impl_for?Elixir.Codeensure_compiled?true__impl__Elixir.Kernelfunction_exported?badargimpl_for
__struct__Elixir.ModuleconcattargetElixir.Enumerable.TupleElixir.Enumerable.AtomElixir.Enumerable.ListElixir.Enumerable.MapElixir.Enumerable.BitStringElixir.Enumerable.IntegerElixir.Enumerable.FloatElixir.Enumerable.FunctionElixir.Enumerable.PIDElixir.Enumerable.PortElixir.Enumerable.Referencebehaviour_info	callbacksoptional_callbacksmodule_info  Code            �   J   � " 0U;U@25BE0@G @@P@@� N  `�r0p00@#@@$�@#@3@@rC@$�q00�� ��0�;�`2������@G�@��@Й0��@�0@@�#@�0q�@�   @@�@@#@@�3@�@q  � �@� ;@��p F G
GEEG � �  � 
@� 00;@�
@
@#@� 0@0F G
G@�  � 
�� 
0 @�  P@� 0A;A@
�@@
#@
 � q@�9!  @
!� 00@;@@�?
@
@#@
!� 0@0A;A@
 �? @
 � ` !0$  @
"� 00@;@@�?
""@
@#@
"� 0@0A;A@
#�?#@
 � p $7'  @
#� 00@;@@�?
%%@
@#@
#� 0@0A;A@
&�?&@
 � � '�*  @
$� 00@;@@�?
((@
@#@
$� 0@0A;A@
)�?)@
 � � *�-  @
%� 00@;@@�?
++@
@#@
%� 0@0A;A@
,�?,@
 � � --0  @
&� 00@;@@�?
..@
@#@
&� 0@0A;A@
/�?/@
 � � 0.3  @
'� 00@;@@�?
11@
@#@
'� 0@0A;A@
2�?2@
 � � 3M6  @
(� 00@;@@�?
44@
@#@
(� 0@0A;A@
5�?5@
 � � 619  @
)� 00@;@@�?
77@
@#@
)� 0@0A;A@
8�?8@
 � � 93<  @
*� 00@;@@�?
::@
@#@
*� 0@0A;A@
;�?;@
 � � <2  @
+� 00@;@@�?
==@
@#@
+� 0@0A;A@
>�?>@
 �  ?@� @0F G
G@�  A� JB� 
,C0B;B@
-D
.ED@G0E@F� 
/ G@� NH� 
/I@@� N     StrT    ImpT   �                                                            !         "         #         $         %         &         '         (         )         *         +               ExpT   |   
   /      I   /       G   ,      C                                             	                  LitT   �   �x�c```a``Hh��lL)<��E�%���9�� !���Ҽ�#3� '>-���q!|���ܤ�"�D&��(5�495�9h��ft��Pj�sN
��kNfEf��k^injQbRN*ns�&�Z� 1A8LocT               Attr  s�l   hd vsnl   n x���l���vA�ޘ��jhd protocoll   hd fallback_to_anyd falsejhd callbackl   hhd countal   hd typek �d funl   hd typek �d productl   hd 	user_typek �d tjjhd typek �d unionl   hd typek  d tuplel   hd atomk  d okhd typek �d non_neg_integerjjhd typek  d tuplel   hd atomk  d errorhd typek �d modulejjjjjjhd callbackl   hhd reduceal   hd typek sd funl   hd typek sd productl   hd 	user_typek sd tjhd 	user_typek sd accjhd 	user_typek sd reducerjjhd 	user_typek sd resultjjjjhd callbackl   hhd member?al   hd typek �d funl   hd typek �d productl   hd 	user_typek �d tjhd typek �d termjjhd typek �d unionl   hd typek  d tuplel   hd atomk  d okhd typek �d booleanjjhd typek  d tuplel   hd atomk  d errorhd typek �d modulejjjjjjj CInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0.1hd timehb  �aaaa/a hd sourcek R/tmp/vagrant/elixir-build.20160602114703.27319/elixir-1.2.5/lib/elixir/lib/enum.exjAbst  �P  4�x��[[��4N�^� $$xĮ�i5�i+<[n�N�M��q����L1v���M;�.��69�~�;�i/p�)��q�(���.v�i?p�!c4gA'pF>Ifa��L �a2#�iK���O��i�s��oΒt]s�0!A&D~�2�Bz�E�9���hF	#>����TX>�q<������>p����dn�?	�8�!���eB�0�q�����3�A��	����A�x�jjn�q�x	]ݔ�s��D� �����>t�՗-�G��ep\�p)J�������'��8�,$�ds[�\�
�s_,�z���uNo�
� �t�n���\nO��!#����]��>C4��n�So�=J�t�p`Y�����j�[o��B�L�,�k���Pd!��QP��a6M��L*�w��H��I4�$�����)��%��k9LRH�������(k��
TAs5հBä�����-����B�0<����p��8�I�tk�
	VV�� ��Qy�s��	�y��c�F�3t�hǤ J	U$��Hd�45�2,*zZ\Rk\R5.��4��I��6�U0R!]�
<����3 �]�\OI����'<�S��x�\cBbq=J�1_�BJU���@Ɍ݃8L����y7*���m�TЁ^�֞�d����똕y�i�n��[G��l�h�Jkr標N� w4����4k+pK�U�=�f��{+(`OS?��;��Rm����>�����Z٠��W�1�z���*���Y�"�Qۖ����~G*v$)$���hnl���R���h$$����ز-��\Z���]���Bk6�Ƃ���&�GM�8��֞*��T���+�YA!�S�Ҽ$x�-�!Qhb>��" ���t���;]�2��� ������]ҙg�]�{�����`E~8�U`E>k���C?����r��� ��:baal������x�YL�xՙ�.��E4E����3f	�#n�����j,�J��\���B���E�(��M�[��mJ��"T� C|���oE���g�g�4���ۨ�	��r!|�~�N{~�=?3e��L���39G���Z#eqi��i1p[N�R$���Ed��2�۾I<���^V�c�%��ʹ�R�%5K5��e鹹�ss)�k����\�Y�Ԣ^�`a����:ع������Sç���B��f.�}Z>IFv4�R`����kC�8���#�5�r�\�bYI�+�vv9E ��{Z���둙x��{h������g�����H�tp�t�z��ϸո�]�We��*����8@���e>�+*��ь�#_�&W�Z�f;��[gȀW�YQKג�?��V%���ˍ��Ɂ��tB洌��$��z�ӌ"P}ջ�k��I��7D1�5W��R*�tE*Ԡh��k��v9w��Τ��9����Ls���$h��q�5
S �nAܬ^�Y�d�"�Cj�O��'�Ik�+�znS��Q�	��:q�Gk��ئM��JQ�P� �sz�[��G�3�.���I�n��f�g�ڢ��0mn`����\��0�	�!ǔoe���3ʯ�l7��u�g�q���]����B�g�w7��xH�p�˙v� n��Uy��x���b���t��L��
�{��sm�멀�<t��/,�.���
	_1G��`�s�ի_�6Gq]��8�T? ��a犫��aG(;$L�lL�Q����7Y&��J��f�Z�b����׍Bb�+��G�2
�o'];�6�2�	�a�)�g��q��{��1������y���bl�&Line   )           @      	t	�	� lib/enum.ex   