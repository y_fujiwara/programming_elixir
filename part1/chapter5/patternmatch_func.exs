handle_open = fn
  {:ok, file} -> "Read data: #{IO.read(file, :line)}"
  {_, error}  -> "Error: #{:file.format_error(error)}"
end

IO.inspect handle_open.(File.open "./exercise-functions1.exs")
IO.inspect handle_open.(File.open "nonexist")
