defmodule MyList do
  # 引数にtotalがあるのが見苦しい
  def sum([], total), do: total
  def sum([head | tail], total), do: sum(tail, head + total)
end
