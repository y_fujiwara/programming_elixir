defmodule Chain do
  # 引数にnext_pidを受けているのが肝
  def counter(next_pid) do
    # Enum以下の段階ではここまで呼ばれる sendするまでreceiveは動かないため
    # IO.puts :erlang.pid_to_list(next_pid)
    receive do
      n ->
        # sendで次に処理するものにメッセージを投げている
        # 再帰っぽい使い方
        send next_pid, n + 1
    end
  end

  def create_processes(n) do
    # selfは初期値 fnの第二引数は前の処理結果
    last = Enum.reduce 1..n, self,
             fn (_, send_to) ->
               # ここでsend_toは前のものを指し示す
               spawn(Chain, :counter, [send_to])
             end
    IO.inspect last

    # spawnしたものはsendして発火される
    send last, 0 # start the count by sending a zero to the last process

    # reduceの初期値はselfなので、最終的にはselfに対してsendされるのでreceiveで受ける
    receive do # and await for the result to come back to us
      final_answer when is_integer(final_answer) ->
        "Result is #{inspect(final_answer)}"
    end
  end

  def run(n) do
    IO.puts inspect :timer.tc(Chain, :create_processes, [n])
  end
end
