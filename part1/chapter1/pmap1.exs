defmodule Parallel do
  def pmap(collection, func) do
    # 非同期処理により,collectionの大きさのプロセスが自動で立ち上がり処理する
    collection
    |> Enum.map(&(Task.async(fn -> func.(&1) end ))) # 起動
    |> Enum.map(&Task.await/1) # 処理?
  end
end

result = Parallel.pmap 1..1000, &(&1 * &1)
IO.inspect result
