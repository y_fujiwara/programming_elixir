defmodule DefaultParams2 do
  # デフォルト引数を指定した関数の宣言のみを書くと複数マッチする関数でもデフォルト引数が使える
  def func(p1, p2 \\ 123)

  def func(p1, p2) when is_list(p1) do
    IO.inspect "you said #{p2} with a list"
  end

  def func(p1, p2) do
    IO.inspect "you passed in #{p1} and #{p2}"
  end

end

