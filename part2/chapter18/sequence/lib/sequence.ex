defmodule Sequence do
  use Application

  # applicationの定義により起動時にプロセス起動して呼ばれる
  def start(_type, _args) do
    # applicationのmodの二個目の要素が第二引数にはいる
    # 今回はenvに定義した変数を使っている
    Sequence.Supervisor.start_link(Application.get_env(:sequence, :initial_number))
  end
end
