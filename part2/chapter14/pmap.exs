defmodule Parallel do
  def pmap(collection, fun) do
    me = self
    # 自分自身に引数の関数を適用して即座にsendし、receiveで受け取る
    collection
    |> Enum.map(fn (elem) ->
         spawn_link fn -> (send me, { self, fun.(elem) }) end
       end)
    |> Enum.map(fn (pid) ->
         receive do { ^pid, result } -> result end
       end)
  end
end
