defmodule MyList do
  def caesar([head], n) do
    [rem(rem((head + n), 123),97) + 97]
  end

  def caesar([head | tail], n) do
    List.flatten [caesar([head], n) | [caesar(tail, n)]]
  end
end
