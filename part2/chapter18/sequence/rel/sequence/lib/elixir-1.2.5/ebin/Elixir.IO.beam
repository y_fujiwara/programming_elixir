FOR1  C\BEAMExDc  �hd elixir_docs_v1l   hd docsl   hhd binreadaaYd defl   hd \\jl   hd devicejd nilhd group_leaderl   hd lineanjd niljhd chars_or_linejd niljm  Reads `count` characters from the IO device, a whole
`:line` or the whole device with `:all`.

It returns:

  * `data` - the input characters

  * `:eof` - end of file was encountered

  * `{:error, reason}` - other (rare) error condition;
    for instance, `{:error, :estale}` if reading from an
    NFS volume

If `:all` is given, `:eof` is never returned, but an
empty string in case the device has reached EOF.

Note: do not use this function on IO devices in unicode mode
as it will return the wrong result.
hhd 	binstreamab  4d defl   hd devicejd nilhd line_or_bytesjd niljm  �Converts the IO device into a `IO.Stream`.

An `IO.Stream` implements both `Enumerable` and
`Collectable`, allowing it to be used for both read
and write.

The device is iterated line by line or by a number of bytes.
This reads the IO device as a raw binary.

Note that an IO stream has side effects and every time
you go over the stream you may get different results.

Finally, do not use this function on IO devices in unicode
mode as it will return the wrong result.
hhd binwriteaa�d defl   hd \\jl   hd devicejd nilhd group_leaderl   hd linea�jjjhd itemjd niljm   �Writes the given argument to the given device
as a binary, no unicode conversion happens.

Check `write/2` for more information.

Note: do not use this function on IO devices in unicode mode
as it will return the wrong result.
hhd chardata_to_stringab  Id defl   hd stringjd niljm  \Converts chardata (a list of integers representing codepoints,
lists and strings) into a string.

In case the conversion fails, it raises a `UnicodeConversionError`.
If a string is given, returns the string itself.

## Examples

    iex> IO.chardata_to_string([0x00E6, 0x00DF])
    "æß"

    iex> IO.chardata_to_string([0x0061, "bc"])
    "abc"

hhd each_binstreamab  �d defl   hd devicejd nilhd whatjd niljd falsehhd each_streamab  �d defl   hd devicejd nilhd whatjd niljd falsehhd getnaa�d defl   hd promptjd nilhd \\jl   hd countjd nilajjm  �Gets a number of bytes from the io device. If the
io device is a unicode device, `count` implies
the number of unicode codepoints to be retrieved.
Otherwise, `count` is the number of raw bytes to be retrieved.
It returns:

  * `data` - the input characters

  * `:eof` - end of file was encountered

  * `{:error, reason}` - other (rare) error condition;
    for instance, `{:error, :estale}` if reading from an
    NFS volume
hhd getnaa�d defl   hd devicejd nilhd promptjd nilhd countjd niljm   �Gets a number of bytes from the io device. If the
io device is a unicode device, `count` implies
the number of unicode codepoints to be retrieved.
Otherwise, `count` is the number of raw bytes to be retrieved.
hhd getsaa�d defl   hd \\jl   hd devicejd nilhd group_leaderl   hd lineb  jjjhd promptjd niljm  �Reads a line from the IO device.

It returns:

  * `data` - the characters in the line terminated
    by a line-feed (LF) or end of file (EOF)

  * `:eof` - end of file was encountered

  * `{:error, reason}` - other (rare) error condition;
    for instance, `{:error, :estale}` if reading from an
    NFS volume

## Examples

To display "What is your name?" as a prompt and await user input:

    IO.gets "What is your name?"
hhd inspectaa�d defl   hd itemjd nilhd \\jl   hd optsjd niljjjm  $Inspects and writes the given argument to the device.

It enables pretty printing by default with width of
80 characters. The width can be changed by explicitly
passing the `:width` option.

See `Inspect.Opts` for a full list of options.

## Examples

    IO.inspect Process.list, width: 40

hhd inspectaa�d defl   hd devicejd nilhd itemjd nilhd optsjd niljm   gInspects the item with options using the given device.

See `Inspect.Opts` for a full list of options.
hhd iodata_lengthab  �d defl   hd itemjd niljm   yReturns the size of an iodata.

Inlined by the compiler.

## Examples

    iex> IO.iodata_length([1, 2|<<3, 4>>])
    4

hhd iodata_to_binaryab  kd defl   hd itemjd niljm  �Converts iodata (a list of integers representing bytes, lists
and binaries) into a binary.

Notice that this function treats lists of integers as raw bytes
and does not perform any kind of encoding conversion. If you want
to convert from a char list to a string (UTF-8 encoded), please
use `chardata_to_string/1` instead.

If this function receives a binary, the same binary is returned.

Inlined by the compiler.

## Examples

    iex> bin1 = <<1, 2, 3>>
    iex> bin2 = <<4, 5>>
    iex> bin3 = <<6>>
    iex> IO.iodata_to_binary([bin1, 1, [2, 3, bin2], 4|bin3])
    <<1, 2, 3, 1, 2, 3, 4, 5, 4, 6>>

    iex> bin = <<1, 2, 3>>
    iex> IO.iodata_to_binary(bin)
    <<1, 2, 3>>

hhd putsaa�d defl   hd \\jl   hd devicejd nilhd group_leaderl   hd linea�jjjhd itemjd niljm   �Writes the argument to the device, similar to `write/2`,
but adds a newline at the end. The argument is expected
to be a chardata.
hhd readaa1d defl   hd \\jl   hd devicejd nilhd group_leaderl   hd lineaCjd niljhd chars_or_linejd niljm  �Reads `count` characters from the IO device, a whole
`:line` or the whole device with `:all`.

It returns:

  * `data` - the input characters

  * `:eof` - end of file was encountered

  * `{:error, reason}` - other (rare) error condition;
    for instance, `{:error, :estale}` if reading from an
    NFS volume

If `:all` is given, `:eof` is never returned, but an
empty string in case the device has reached EOF.
hhd streamab  d defl   hd devicejd nilhd line_or_codepointsjd niljm  OConverts the io device into a `IO.Stream`.

An `IO.Stream` implements both `Enumerable` and
`Collectable`, allowing it to be used for both read
and write.

The device is iterated line by line if `:line` is given or
by a given number of codepoints.

This reads the IO as utf-8. Check out
`IO.binstream/2` to handle the IO as a raw binary.

Note that an IO stream has side effects and every time
you go over the stream you may get different results.

## Examples

Here is an example on how we mimic an echo server
from the command line:

    Enum.each IO.stream(:stdio, :line), &IO.write(&1)

hhd writeaa�d defl   hd \\jl   hd devicejd nilhd group_leaderl   hd linea�jjjhd itemjd niljm   �Writes the given argument to the given device.

By default the device is the standard output.
It returns `:ok` if it succeeds.

## Examples

    IO.write "sample"
    #=> "sample"

    IO.write :stderr, "error"
    #=> "error"

jhd 	moduledocham  �Functions handling IO.

Many functions in this module expect an IO device as an argument.
An IO device must be a pid or an atom representing a process.
For convenience, Elixir provides `:stdio` and `:stderr` as
shortcuts to Erlang's `:standard_io` and `:standard_error`.

The majority of the functions expect char data, i.e. strings or
lists of characters and strings. In case another type is given,
functions will convert to string via the `String.Chars` protocol
(as shown in typespecs).

The functions starting with `bin*` expect iodata as an argument,
i.e. binaries or lists of bytes and binaries.

## IO devices

An IO device may be an atom or a pid. In case it is an atom,
the atom must be the name of a registered process. In addition,
Elixir provides two shorcuts:

  * `:stdio` - a shortcut for `:standard_io`, which maps to
    the current `Process.group_leader/0` in Erlang

  * `:stderr` - a shortcut for the named process `:standard_error`
    provided in Erlang

IO devices maintain their position, that means subsequent calls to any
reading or writing functions will start from the place when the device
was last accessed. Position of files can be changed using the
`:file.position/2` function.

hd callback_docsjhd 	type_docsl   hhd chardataa a'd typed nilhhd devicea a%d typed nilhhd nodataa a&d typed niljj Atom  �   C	Elixir.IO__info__	functionsmacroserlangget_module_infodo_read_allioget_linebit_sizealleofbinwritegroup_leaderreadeach_binstreamerrorreasonElixir.IO.StreamError	exceptionhaltiodata_lengthiolist_sizegetsputsstdiostderrstandard_iostandard_errorElixir.String.Chars	to_string	put_charsgetn	get_charschardata_to_stringunicodecharacters_to_binary
incompleterestencodedElixir.UnicodeConversionErroreach_streaminspectElixir.Inspect.OptsElixir.KernelstructElixir.Inspect.Algebrato_docwidthbadkeyformatiodata_to_binaryiolist_to_binarylinebinreadfile	read_lineokwritedo_binread_allstreamfalseElixir.IO.Stream	__build__	binstreamtruemodule_info   Code  �          �   �   � " 0U;U@25BE0@G @@P@@� N  `�r p  @@G@�  5��0@|  #o#� � Z�� @ u �+��@ � ��@��@�@ 0@ ���P��@�P 0@ A��`
 � @�p Z9: B B#+
P0F G
G#E��@��P+�0 F G
GPEF GG��
��N`��
@�� 0@ ���
   @0;@

@
=@
=0=1=9!@7@= 5@= @��p  EG @�� � ! F#G��J#"�
!0#00@@#$0';&@
$
%$@
=)%@
=)&0'=)'1(=)(9-)@7*@=,*5+@=,+@��p,@@$#@�0�0- F3G��J3.�
#/5007. ��51 194:40B B#B 304;4@
2
&32�@F G
'G3EG0F 3G
(G#E3���P3�@F G
'G3EG@F 3G
(G#E3���P4�J5�
* 6 @� A97:8 B B#+8
P0F G
G#E�@�P7+8�0 F G
G8PEF GG9�
+0:79#@0@$@4@#@
,� �@@4@� �@�<�; 
1@==;�<@ F0G
2G
1G�P<@
1@� p =@@�  � �@@�� @>�
4?�N�@�� A0P;P@�B
6IB 0F;E@
C
DC@
=HD@
=HE0F=HF1G=HG9WH@G u I 0M;L@
J
KJ@
=OK@
=OL0M=OM1N=ON9WO@�   P(@ @0T;S@
Q
RQ@
=VR@
=VS0T=VT1U=VU9XV@#@�!0�W FG��JX F#G��J#Y�"
7 Z0j;j@�[
6b[ 0_;^@
\
]\@
=a]@
=a^0_=a_1`=a`9ra@G � b 0f;e@
c
dc@
=hd@
=he0f=hf1g=hg9rh�#9i:i B B#+i
:@# i j(Y @0n;m@
k
lk@
=pl@
=pm0n=pn1o=po9sp@�$ 9q:q B B#+q
:@#qr FG��Js F#G��J#t�%
+u@ �v�&
; w  @0{;z@
x
yx@
=}y@
=}z0{=}{1|=}|9�}@7~@=�~5@=�@��p�@@�' � � F#G��J#��(
< �  @@ @�) 9�:� B B#+�
:�*@|0 #o� 0� Z�� #@@ � �+��@ � ��+
= � @0�;�@
�
��@
=��@
=��0�=��1�=��9��@#@
>�,0� F#G��J#��-
7�@�- 0@ Z��.
!�@ ���&
;�@�& 0@ w��%
+ �  @@�/ 0@#@0: ���
�@�� 0@ ���
 �  @0�;�@
�
��@
=��@
=��0�=��1�=��9��@7�@=��5�@=��@��p�@@�0  � F#G��J#��1
! �-�  @@�2 0@#@0# �@#0#��@� �7�=��5�� @0�;�@
�
��@
=��@
=��0�=��1�=��9��@�3 � F#G��J#��4
A � @0�;�@
�
��@
=��@
=��0�=��1�=��9��@#@
B�50� F#G��J#�� 
C �@� N�� 
C�@@� N   StrT    ImpT                     	         
                                                                 "      $   %      )         -   .      /   0      /   3         5      8   9      8         ?   @      8   ;            ExpT  T      C      �   C       �   A      �         �   !      �         �         �   +      �   ;      �   !      �   7      �   =      �   ;      w   +      u   7      Z         A   4      ?   +      :   *      6   #      /   !      #                                                               LitT   �  �x�eQ��0\��b4ƛ&~����(��3?��-������t !��� .��!�K�ƙ�b��v���!�E��$v�t�Y�z�4Ku��T9鎂eE��7�e%ȅV4ApQ��pp��1�v"��3[���A�&^-T�ҟl#c@ֿ���^e eq�
ܖ��#��~,�K�u46@'+�W�	�VI�M�'�%�n�D��m�ZhQ~ 7�~�  LocT         <      �         Attr   (�l   hd vsnl   n $t���
��/��-^jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0.1hd timehb  �aaaa/a%hd sourcek P/tmp/vagrant/elixir-build.20160602114703.27319/elixir-1.2.5/lib/elixir/lib/io.exj  Abst  ��P  Xhx��Ms�uW+���� �C�r�}�@U9$� �*H��}�T1�vF�,�3��Y>�� S���I�9X(T���A�7���U������_�~�ܴݯ��{��u�G��w�\;�X-v�l�J�?.���	Ǒ?׉=VrK㵰���[���ġ�7[a+�e�4:��GΖ&�ܻ~�߻��:X���pb��_��G?Ёr���Ӫ4������M�r\�G����Y䲘�r]A�{�]�:��V�#YU����P]�iA&���(�Җp�F�b�`D@$cbr��a���GY6�h����z�BH���6���;�F��JF/��(���؋��0�x���;��B'����k���h��#�V�q�d-t=4X`Ѕ����[^m1u??bVI-R���*�Ux>:ފ�c������"'���H\9Ǣ��{MM��uYve��W���0r�\9��u��xs\�w��U�k�,`�˘M@1G��D!��z`TD�
���?��/��JJ�t=�*��t�iz�kx���A����7��I7G���A7G��͑�ts��f	�f��d�'M�%]de]EK&-ai��)�z]�H��A�-�n�Չ۪���Z�%�5[�A%��2�hA��n��� �\�`a�_ۦL�F³���b5,x�20�0{���^��$�7k%\1��A�d�G)�R�[Y4�`!^�B��I�7l!p�|bwN�-��Ǳp�8v>�����`�e�[T0ܲ�-k0��ł��"%��y���*��U�V)=�RF������jZ-�&��O��"ទ"?��������Ϭ�6|��C^��r��)����b�3��V�*>��������$r��9Q���xҔO9�㓨RW+���('u��t�iW%0����w����8�x��j��0�Q���v�
��P\,T�5�bQ,5lj����xk�cO�������v5)5�P������nS�|�C�j��'LU5�I��WU����Α��9G�X5!���F=#o�l���۔q�m>�mM��ç�ms��1]L����V�h��0'?:Nڮ*
�����)+Ő1���]�-:
��� IM_�py)�	r�2�t��l��S��� 7�A�F}P%�a�L����חI�/�
~1#]h��C>�FA��Ƌ�&L=CNpNj1G���MV�B<Vi�P��+���?�9�u#����0ʆ<Yv��c9I�m�.0Xw�\a�E��bu�+_sc��]!��1n�\�I;;CE Tk�Ý�Raz�F�ץj�7+	���t%>�.s�X)�W�a�w2����A�bB�D��%]�L��cI���Pˇ�^�oy{8���}0�o?C�ax��!�6�E�j���ؓ_��~|r��I-�zu؀2_��9}^�T�!���VV3�[V�u��A�M�h��1�,�����)璎5�r:��#�Iˑd�%v%y�Wv�y�u�ў��`&;y1>�g+���&k�<W\�wN��Z���X@�ᄔ�������}��(䔷�9�}���.*8f�	f����yL&C�B�f��&�v��T4[�"P�X�Y���)�q'��x��q�7_���;��ͳN#-,卑}qƺqu�A��?҃#���:��p��c�1'Ƌ^$}��R}x��4^�����M�m֤�,�3(�$kv'�h�\��N��<��53��"�TqF���Q�)�@�M�a
�0e�a�oD���+��m3i $P��*�Bt�&�������8�Ty¢�)��*���~��n��R��9��XzNJz^�w��s��*�ȇ
BUw�#�ج8�v�`S��?ٌv��1�B�[�S�S����K��	nm�e�'���.s�"�TjI��V��ܑ��~��W�/�X�i1���^Eb��ӵFm�@mURS7r1-Ӹi�����CZ뀇��_��1��(��(���0 6~pG�;]Sם�N��~]T��Ӹ'L��B�ޅ�׊�Sj*&��Ún��|����{��1q�X����BIP�6�"�sӻ���ϟ�ێ�k껯�����fWC��ۥ���i��4z7=]�I7o,S�O�V6$���-.R8,�dW'���6\�x	TB�X� �D&J$k���$w��"
�FVd�])�]�:+�.t/�t�V���&���KJ�6�gSg*u~@⁕�qS*A	"p�}C�Wi��]|���sq�����2�lEa����Z�	�m��
j�Y{��mh�q@0�x�h��f��r��L�S�gji|JK|��t��8>��婮qy�ZO!�DQ�78�HpZ�>ݵ�?���+��-�=�G�/"�(�DZ{8����}�{Q[65F,�Z؜��Iv�6?|�uuFa�LW]����$l��"AMٷ�1|׬�L5���Yׂ�D6�v��"��Ux�\Jّ�.+�>N=`*�qk��K*w�j<�ԁ��Ws�+xr]���`=��k�����'a^w�7�@��6��	�3:>�L�"�aA1�BWoX0{�Yް�z�a*goX�����CP�&���ʀ���n��~�l�,p�6��y��V=G�MMa�U`J|AEvOˁ�@���1񀵕T��k�=z:�=���u��{�f�7L��O��ɴ|����S)�"�{���mY}�C�����Ĳ�-�lm�� 5� WP��\1�Z�w@��:; ��u@���E��m�Knw����DmG�C��k������_���K�+k�=ҿ��G�|��:�=*Ηh(l�m��#x�{d��Q�C��F���q�8;$�?��#�o/A���d�7����=��q���Z�O�e��V1�%R`���pr����F���Ƃ71	�1V�È�M����Í�p�bK7�V��z�%�6��WWz�u��{G&=��l�p�ݢ�����/��Ɩx�����F��5��v%�]uĕLO���/]�X�nzYˣ����_��|v�H[x���sK9}��s�=�]��������q!yB���D�O$�7m��xB� O@�4 �W�>�f�fo�״���~])	�w�/�'�$�ws�溱�.��G�LŦ������3������މ �f�jvV��z�q�{��o�����1���Ը=�=�( -�1$
���$�cIާ��}ЫՎrbv�����蝗4E��8nV�qK�8�5q���75���W�
	9n��՗�~"A>�'l7	*��bXZ�Oz�]������;���^��G_t���rO.C,���js��Ih�)D|5��;���� ������B8.a�_�"�5�.�H��k�q◥�/C�]����K���� h�]�\1E�!+~ڧ?5[�Sڊ�a���VDSjFx �BҎ�0&�˳R0��H��r鸬~�X�}F5mĠ�H`+�QV�`v^�ogF�ʊK�/����Xq(ڹ��UBMW�����^1>ڽ*��
�v�]W���ը���֞��tw���Y�C�`I�Z2lJ ��v�n�vv	������}�oemlj&���o�c���qݒ5Dm|�U�k.�h����'�'�R'�q��
����B��R��]~���C%?�����u���8��¨2`2��9����<�~k�R��8�1��P�c� 	������������jb�أ?Jrl���
e˖�s��#7˒����e��e�J��W)ܥ��� ��ޞ{ ���p%c��]�@@��v��{�3�6t��t�m�h��U�M��l�.��@�7zd��Բ�!KNGDfܧd�}�'�5����֘
�����X��x�tƌ2��ϱ�?�ޝ=�"�<�N�����x�_�>G'o����Hi�xag��_Px9���	��'�����y���%+_��B�RaҚ��W>z�[�"�Q��dY��D��k�~?��o� Ձ1��5�5��C붲}ݰ��#���N��׍-S�����qZˈj���K+��
%㌒g�B�%㌽d�Q��yR��J�$=|��U��[\��  Line   �           [   5   	Q	R	S	�	C)�)�)�)�)�)	�)�	�)�	�	�)Z)_)d)g)�)�)�	�	�	�	�)�)�	E	J	N	p	u	|	�	�	�	�	�	�)0)1	n	�	�)	�	�	�)E)F 	lib/io.ex  