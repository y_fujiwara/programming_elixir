defmodule Times do
  def double(n) do
    n * 2
  end

  def triple(a, b, c) do
    a * b * c
  end

  def quadruple(n) do
    double(n) * double(n)
  end
end
