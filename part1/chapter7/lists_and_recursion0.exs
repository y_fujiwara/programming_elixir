defmodule Exercise0 do
  def sum([max]), do: max
  def sum([head | tail]), do: sum([head]) + sum(tail)
end
