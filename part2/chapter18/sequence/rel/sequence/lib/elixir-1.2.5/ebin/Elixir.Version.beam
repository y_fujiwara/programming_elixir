FOR1  #BEAMExDc  ��hd elixir_docs_v1l   hd docsl   hhd 
__struct__a aFd defjd nilhhd compareaa�d defl   hd version1jd nilhd version2jd niljm  Compares two versions. Returns `:gt` if first version is greater than
the second and `:lt` for vice versa. If the two versions are equal `:eq`
is returned

Raises a `Version.InvalidVersionError` exception if `version` is not parsable.
If given an already parsed version this function won't raise.

## Examples

    iex> Version.compare("2.0.1-alpha1", "2.0.0")
    :gt

    iex> Version.compare("2.0.1+build0", "2.0.1")
    :eq

    iex> Version.compare("invalid", "2.0.1")
    ** (Version.InvalidVersionError) invalid

hhd match?aagd defl   hd versionjd nilhd requirementjd niljm  �Checks if the given version matches the specification.

Returns `true` if `version` satisfies `requirement`, `false` otherwise.
Raises a `Version.InvalidRequirementError` exception if `requirement` is not
parsable, or `Version.InvalidVersionError` if `version` is not parsable.
If given an already parsed version and requirement this function won't
raise.

## Examples

    iex> Version.match?("2.0.0", ">1.0.0")
    true

    iex> Version.match?("2.0.0", "==1.0.0")
    false

    iex> Version.match?("foo", "==1.0.0")
    ** (Version.InvalidVersionError) foo

    iex> Version.match?("2.0.0", "== ==1.0.0")
    ** (Version.InvalidRequirementError) == ==1.0.0

hhd parseaa�d defl   hd stringjd niljm   �Parses a version string into a `Version`.

## Examples

    iex> {:ok, version} = Version.parse("2.0.1-alpha1")
    iex> version
    #Version<2.0.1-alpha1>

    iex> Version.parse("2.0-alpha1")
    :error

hhd parse_requirementaa�d defl   hd stringjd niljm   �Parses a version requirement string into a `Version.Requirement`.

## Examples

    iex> {:ok, req} = Version.parse_requirement("== 2.0.1")
    iex> req
    #Version.Requirement<== 2.0.1>

    iex> Version.parse_requirement("== == 2.0.1")
    :error

jhd 	moduledocham  �Functions for parsing and matching versions against requirements.

A version is a string in a specific format or a `Version`
generated after parsing via `Version.parse/1`.

`Version` parsing and requirements follow
[SemVer 2.0 schema](http://semver.org/).

## Versions

In a nutshell, a version is represented by three numbers:

    MAJOR.MINOR.PATCH

Pre-releases are supported by appending `-[0-9A-Za-z-\.]`:

    "1.0.0-alpha.3"

Build information can be added by appending `+[0-9A-Za-z-\.]`:

    "1.0.0-alpha.3+20130417140000"

## Struct

The version is represented by the Version struct and fields
are named according to Semver: `:major`, `:minor`, `:patch`,
`:pre` and `:build`.

## Requirements

Requirements allow you to specify which versions of a given
dependency you are willing to work against. It supports common
operators like `>=`, `<=`, `>`, `==` and friends that
work as one would expect:

    # Only version 2.0.0
    "== 2.0.0"

    # Anything later than 2.0.0
    "> 2.0.0"

Requirements also support `and` and `or` for complex conditions:

    # 2.0.0 and later until 2.1.0
    ">= 2.0.0 and < 2.1.0"

Since the example above is such a common requirement, it can
be expressed as:

    "~> 2.0.0"

`~>` will never include pre-release versions of its upper bound.
It can also be used to set an upper bound on only the major
version part. See the table below for `~>` requirements and
their corresponding translation.

`~>`           | Translation
:------------- | :---------------------
`~> 2.0.0`     | `>= 2.0.0 and < 2.1.0`
`~> 2.1.2`     | `>= 2.1.2 and < 2.2.0`
`~> 2.1.3-dev` | `>= 2.1.3-dev and < 2.2.0`
`~> 2.0`       | `>= 2.0.0 and < 3.0.0`
`~> 2.1`       | `>= 2.1.0 and < 3.0.0`

hd callback_docsjhd 	type_docsl   	hhd builda aNd typed nilhhd majora aJd typed nilhhd 	matchablea aOd typed nilhhd minora aKd typed nilhhd patcha aLd typed nilhhd prea aMd typed nilhhd requirementa aId typed nilhhd ta aSd typed nilhhd versiona aHd typed niljjAtom  �   )Elixir.Version__info__	functionsmacroserlangget_module_infoparse_requirementElixir.Version.Parserok	matchspec
__struct__Elixir.Version.Requirementsourceerrorcompareparseparse_versionbuildminormajorprepatch	get_buildElixir.Regexrunnilmatch?message&Elixir.Version.InvalidRequirementError	exceptionetstest_ms=/=false
do_comparegtlteqto_matchable"Elixir.Version.InvalidVersionErrormodule_info   Code  �          �   -   � " 0U;U@25BE0@G @@P@@� N  `�rp5e@� 9�:� B B#+���0�G0`�#���00F G�G@�+���� J��@� � @�P%@@@�P%@@ ��`
�5�@@4�p 9�:� B B#+��9�#:�#@B# 3B#$B# B#0@4@34�����G �
�
$
4

0F G�G@@�+��@�pJ��
 @@G0�� 08A#8#A#343@ +
 ��J��� @G@��
 5  @@@��u9: B B#+�@#@  +�P F G
GE��@��P��@�3�#+3�0@#�%@� � `9: B B#+�p#
" ��J�H�
# 9:@B #B3B CB0S9:@ �`B cBsB �B0�F0G#G3GCF0GcGsG�( ��F0G#G3GCF0GcGsG�("@S@�+S+ �,,"'! @
$ !'#"@
% #@
& $�
'%�&�&��S
C
3
#
+&SPPF@GCG3G#G&@� 9':( B B#+(�@#'+(�P F G
GE���P(�J)� 
) *@� N�+� 
),@@� N   StrT    ImpT   |   
                                                                      !      (               ExpT   d      )      ,   )       *                                                       LitT    x�}��J1�'mw�W�[��B�7P��ez��fpS��Iz}:_�q�l+��������  �]����a_J�I��
|a/��Z��Wa�\�]�D�u�J�|���1��N�9�������x������5U9I6mL���9��a�����5��.4��ق9`�P��9�2��
x�M��k?�ې�][�U�%�W�O�7�xn������4��� ��B[5����`��=��}t>b�u�w��ccr��_�A��m�)�����ׄ���.   LocT   (      '      %   #               Attr   (�l   hd vsnl   n �+��M�:������1jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0.1hd timehb  �aaaa/a0hd sourcek U/tmp/vagrant/elixir-build.20160602114703.27319/elixir-1.2.5/lib/elixir/lib/version.exj Abst  	�P  1�x���nǵg�&�V|�%A.�' Y��	�BI�'@�4�3E�G=ݣ��:���@�/d
���HI	�	�\H�4b �l��z����{zH�� ��۷z��L�e�	�_m2FiF��ײ�wg�-k�DQ�L�"J��5��;]ǥ-�-ϷI/�m����HA����Û�޶�r���4�;G�HE������z�u��ܧ1���ۖu��qqvt�K�����HXm��r����9�P/ҩ��PuH�t�����g¿���%QsF����u�c_��>�sܖ�#�#��t2n��T�dQ�s����G@��P�c Σ�qXl:~D��c�qp�D~����I��b�i�Q%j��{!�?�#S
&�T�r�b�W����?74S�i6]6ũ*��Rl͕c�9c}��=ۣӶ�Et��nW�n�,$��X7�#=j��j��AI��$�k9��K�8��KQ�cu�	��.2�xq�O�qc�A�F���z�r׏����ԑ�F��>�z]W��:�%��s#S�6K���<(��#�ob��0-}W�?����d�\����2� u5D�p]t-�uH�3F�3jO9�m�$�f&>�)�f��=�6�+or{@�2πaL��ٙ��;�A���{�ٙr�S���3��rS�	#��9SMf� dÕ҂Rm���?�lk&-K��'Wq7v]�76���V�~bh*C4|�c3�<1,:ˇ���b��y슪�Ƽ�a�IcO��j���$���>Dڊ	���NU��Q݇��>�W~�Fe�pī�x��;�o�0hm#��Yi��[��m����2t�$�}�-��@]����R<z�M�yu�fo��o�D᠑���&�c��	+Q��XQ�?���`�S����]c�`�1�I3b�1�9��V�~� ���~�ș�6i��4��T�ɥJ� ����0�p�ﮪWF]���݈b��+yZ�����	�$C�#7]�Z�����[�ݖh��Y��Rec��A%ՋX��Y�IZ
�jyI�A\{
}�<�^�:�j<zs��#)�4������r@�2*��p�c+��D��g�id�u&RH7U��(������1��P<]��R,{-{Qlc6�����n��>�Ԏ���� y�(O�Mnn QvL.�j��V�o�ʐ�s0y��)�I�h�L^�+^]����R[J�C۟�p���&�Ly	S�<^^Ż8$��a�J2>����N��OԢ�R?��/��ʐO%��f�������#��#����D�|�xoD���p
?Bj�O�Ah�zeS]��O61��~�c��fq���o���cK�ۂ�j[l���G���Va�xZ����ٍG��RQl� ����
L��6�V�8�ٴ3��Y����71-!��'1���KJpJN@���2� �� ���8ufbq�HI��>/#U^E�>��26c|8�Ä{}�i��؂s��Fh�w�_�����j9�Е�T��l�k���_�~���C�C�n�&o$n�`����Iz��,j�08,c,�?��ߍW�ﶖ9���Y�D4�@&͇��x�n/��c�� �f��M����q`�uۊ.N||3�⟷ٿ��笀Tؿ�����Y������U�}������Z���O�}�J��鹑��]ƃp���ӟ��ş��Y�糿|7Gv����o���ڋ��ȁC�oM���m˪�9��rc5+��6.��K$�h/)��j/Av��.e�h����3�)i�ZW*A�=��S�o����8(e��)���lA9?�\�����a�7���3W.�ƻP<W.�ϕ�0��ߓB�I������� �|>����²�������~���F���yCU��o��4x C�84R.�y�\�/{��uZ`S��"ct�I�Q6ӗ��uh�i9�=�b
[���.b�/���l��G�H3"��V�ż��D��[ʖ�'������%�%�Q(�c3������	�]y	!l�Kh���n��U������2�/��8秈R��=F����8sU��x�cF�k /��
=\��|!1����I�
2����G�#u�2~TXξ <�Mf.��\f�K0l����&�/�ݭΊ��f��t��|���,Qܴ��i������lՊ�M+&7��)�a��P��`j��\���nZMݴ�: �Z����NW�ė}���PU3p�ޓl<�$�-]5�Kh�Q�<:�OQ�izWlͯk��5]�5��Z�_�p���U׏�8�_���Q֯JҮ���z*z=�~y]�P1_iZ?A�(]Z��izm�n�jW��������i����*o|�A����p7�V���ݜ�L8c7�Bi&F�����_(ws/�&bt��͹P��������g8c���{�3��<c�~�k���ܾ�᜹����M�~�7��W�o����R�>�?�rJ/d���A�BV��g�,�SY�^�2v��xk��C��u   Line   M           $      	�	�	�	�	�	�	�	�	�	�	�	F	�	�	�	�	�	�	�	� lib/version.ex   