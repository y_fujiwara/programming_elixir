FOR1  S�BEAMExDc  1L�hd elixir_docs_v1l   hd docsl   hhd after_spawna b  :d defjm   ,Returns registered `after_spawn` callbacks.
hhd after_spawnab  3d defl   hd funjd niljm   ERegisters a function to be invoked after the IEx process is spawned.
hhd colorab  Hd defl   hd colorjd nilhd stringjd niljm   nReturns `string` escaped using the specified `color`.

ANSI escapes in `string` are not processed in any way.
hhd configurationa b  ,d defjm   Returns IEx configuration.
hhd 	configureaa�d defl   hd optionsjd niljm  	�Configures IEx.

The supported options are: `:colors`, `:inspect`, `:width`,
`:history_size`, `:default_prompt` and `:alive_prompt`.

## Colors

A keyword list that encapsulates all color settings used by the
shell. See documentation for the `IO.ANSI` module for the list of
supported colors and attributes.

The value is a keyword list. List of supported keys:

  * `:enabled`      - boolean value that allows for switching the coloring on and off
  * `:eval_result`  - color for an expression's resulting value
  * `:eval_info`    - ... various informational messages
  * `:eval_error`   - ... error messages
  * `:stack_app`    - ... the app in stack traces
  * `:stack_info`   - ... the remaining info in stacktraces
  * `:ls_directory` - ... for directory entries (ls helper)
  * `:ls_device`    - ... device entries (ls helper)

When printing documentation, IEx will convert the markdown
documentation to ANSI as well. Those can be configured via:

  * `:doc_code`        - the attributes for code blocks (cyan, bright)
  * `:doc_inline_code` - inline code (cyan)
  * `:doc_headings`    - h1 and h2 (yellow, bright)
  * `:doc_title`       - the overall heading for the output (reverse, yellow, bright)
  * `:doc_bold`        - (bright)
  * `:doc_underline`   - (underline)

## Inspect

A keyword list containing inspect options used by the shell
when printing results of expression evaluation. Default to
pretty formatting with a limit of 50 entries.

See `Inspect.Opts` for the full list of options.

## Width

An integer indicating the number of columns to use in documentation
output. Default is 80 columns or result of `:io.columns`, whichever
is smaller. The configured value will be used unless it is too large,
which in that case `:io.columns` is used. This way you can configure
IEx to be your largest screen size and it should always take up the
full width of your terminal screen.

## History size

Number of expressions and their results to keep in the history.
The value is an integer. When it is negative, the history is unlimited.

## Prompt

This is an option determining the prompt displayed to the user
when awaiting input.

The value is a keyword list. Two prompt types:

  * `:default_prompt` - used when `Node.alive?` returns `false`
  * `:alive_prompt`   - used when `Node.alive?` returns `true`

The following values in the prompt string will be replaced appropriately:

  * `%counter` - the index of the history
  * `%prefix`  - a prefix given by `IEx.Server`
  * `%node`    - the name of the local node

hhd dont_display_resulta b  �d defjd falsehhd inspect_optsa b  _d defjm   &Gets the options used for inspecting.
hhd pryab  fd defmacrol   hd \\jl   hd timeoutjd nilb  �jjm  �Pries into the process environment.

This is useful for debugging a particular chunk of code
and inspect the state of a particular process. The process
is temporarily changed to trap exits (i.e. the process flag
`:trap_exit` is set to `true`) and has the `group_leader` changed
to support ANSI escape codes. Those values are reverted by
calling `respawn`, which starts a new IEx shell, freeing up
the pried one.

When a process is pried, all code runs inside IEx and, as
such, it is evaluated and cannot access private functions
of the module being pried. Module functions still need to be
accessed via `Mod.fun(args)`.

## Examples

Let's suppose you want to investigate what is happening
with some particular function. By invoking `IEx.pry/1` from
the function, IEx will allow you to access its binding
(variables), verify its lexical information and access
the process information. Let's see an example:

    import Enum, only: [map: 2]
    require IEx

    defmodule Adder do
      def add(a, b) do
        c = a + b
        IEx.pry
      end
    end

When invoking `Adder.add(1, 2)`, you will receive a message in
your shell to pry the given environment. By allowing it,
the shell will be reset and you gain access to all variables
and the lexical scope from above:

    pry(1)> map([a, b, c], &IO.inspect(&1))
    1
    2
    3

Keep in mind that `IEx.pry/1` runs in the caller process,
blocking the caller during the evaluation cycle. The caller
process can be freed by calling `respawn`, which starts a
new IEx evaluation cycle, letting this one go:

    pry(2)> respawn
    true

    Interactive Elixir - press Ctrl+C to exit (type h() ENTER for help)

Setting variables or importing modules in IEx does not
affect the caller the environment (hence it is called `pry`).
hhd pryab  �d defl   hd bindingjd nilhd envjd nilhd timeoutjd niljm  Callback for `IEx.pry/1`.

You can invoke this function directly when you are not able to invoke
`IEx.pry/1` as a macro. This function expects the binding (from
`Kernel.binding/0`), the environment (from `__ENV__`) and the timeout
(a sensible default is 5000).
hhd startab  �d defl   hd \\jl   hd optsjd niljjhd \\jl   hd mfajd nilhd {}l   hd lineb  �jl   hd __aliases__l   hd countera hd lineb  �jl   d IExjd dont_display_resultjjjjd falsehhd started?a b  Ad defjm   #Returns `true` if IEx was started.
hhd widtha b  Vd defjm   XGets the IEx width for printing.

Used by helpers and it has a maximum cap of 80 chars.
jhd 	moduledocham  YElixir's interactive shell.

This module is the main entry point for Interactive Elixir and
in this documentation we will talk a bit about how IEx works.

Notice that some of the functionalities described here will not be available
depending on your terminal. In particular, if you get a message
saying that the smart terminal could not be run, some of the
features described here won't work.

## Helpers

IEx provides a bunch of helpers. They can be accessed by typing
`h()` into the shell or as a documentation for the `IEx.Helpers` module.

## Autocomplete

To discover all available functions for a module, type the module name
followed by a dot, then press tab to trigger autocomplete. For example:

    Enum.

Such function may not be available on some Windows shells. You may need
to pass the `--werl` flag when starting iex, as in `iex --werl` for it
to work. `--werl` may be permanently enabled by setting the `IEX_WITH_WERL`
environment variable.

## The Break command

Inside IEx, hitting `Ctrl+C` will open up the `BREAK` menu. In this
menu you can quit the shell, see process and ets tables information
and much more.

## The User Switch command

Besides the break command, one can type `Ctrl+G` to get to the
user switch command menu. When reached, you can type `h` to
get more information.

In this menu, developers are able to start new shells and
alternate between them. Let's give it a try:

    User switch command
     --> s 'Elixir.IEx'
     --> c

The command above will start a new shell and connect to it.
Create a new variable called `hello` and assign some value to it:

    hello = :world

Now, let's roll back to the first shell:

    User switch command
     --> c 1

Now, try to access the `hello` variable again:

    hello
    ** (UndefinedFunctionError) undefined function hello/0

The command above fails because we have switched shells.
Since shells are isolated from each other, you can't access the
variables defined in one shell from the other one.

The user switch command menu also allows developers to connect to remote
shells using the `r` command. A topic which we will discuss next.

## Remote shells

IEx allows you to connect to another node in two fashions.
First of all, we can only connect to a shell if we give names
both to the current shell and the shell we want to connect to.

Let's give it a try. First start a new shell:

    $ iex --sname foo
    iex(foo@HOST)1>

The string between the parentheses in the prompt is the name
of your node. We can retrieve it by calling the `node()`
function:

    iex(foo@HOST)1> node()
    :"foo@HOST"
    iex(foo@HOST)2> Node.alive?()
    true

For fun, let's define a simple module in this shell too:

    iex(foo@HOST)3> defmodule Hello do
    ...(foo@HOST)3>   def world, do: "it works!"
    ...(foo@HOST)3> end

Now, let's start another shell, giving it a name as well:

    $ iex --sname bar
    iex(bar@HOST)1>

If we try to dispatch to `Hello.world`, it won't be available
as it was defined only in the other shell:

    iex(bar@HOST)1> Hello.world
    ** (UndefinedFunctionError) undefined function Hello.world/0

However, we can connect to the other shell remotely. Open up
the User Switch prompt (Ctrl+G) and type:

    User switch command
     --> r 'foo@HOST' 'Elixir.IEx'
     --> c

Now we are connected into the remote node, as the prompt shows us,
and we can access the information and modules defined over there:

    rem(foo@macbook)1> Hello.world
    "it works"

In fact, connecting to remote shells is so common that we provide
a shortcut via the command line as well:

    $ iex --sname baz --remsh foo@HOST

Where "remsh" means "remote shell". In general, Elixir supports:

  * remsh from an Elixir node to an Elixir node
  * remsh from a plain Erlang node to an Elixir node (through the ^G menu)
  * remsh from an Elixir node to a plain Erlang node (and get an `erl` shell there)

Connecting an Elixir shell to a remote node without Elixir is
**not** supported.

## The .iex.exs file

When starting IEx, it will look for a local `.iex.exs` file (located in the current
working directory), then a global one (located at `~/.iex.exs`) and will load the
first one it finds (if any). The code in the chosen .iex.exs file will be
evaluated in the shell's context. So, for instance, any modules that are
loaded or variables that are bound in the .iex.exs file will be available in the
shell after it has booted.

Sample contents of a local .iex.exs file:

    # source another ".iex.exs" file
    import_file "~/.iex.exs"

    # print something before the shell starts
    IO.puts "hello world"

    # bind a variable that'll be accessible in the shell
    value = 13

Running the shell in the directory where the above .iex.exs file is located
results in:

    $ iex
    Erlang 17 [...]

    hello world
    Interactive Elixir - press Ctrl+C to exit (type h() ENTER for help)
    iex(1)> value
    13

It is possible to load another file by supplying the `--dot-iex`
option to iex. See `iex --help`.

## Configuring the shell

There are a number of customization options provided by the shell. Take a look
at the docs for the `IEx.configure/1` function by typing `h IEx.configure/1`.

Those options can be configured in your project configuration file or globally
by calling `IEx.configure/1` from your `~/.iex.exs` file like this:

    # .iex.exs
    IEx.configure(inspect: [limit: 3])

    ### now run the shell ###

    $ iex
    Erlang 17 (erts-5.10.1) [...]

    Interactive Elixir - press Ctrl+C to exit (type h() ENTER for help)
    iex(1)> [1, 2, 3, 4, 5]
    [1, 2, 3, ...]

## Expressions in IEx

As an interactive shell, IEx evaluates expressions. This has some
interesting consequences that are worth discussing.

The first one is that the code is truly evaluated and not compiled.
This means that any benchmarking done in the shell is going to have
skewed results. So never run any profiling nor benchmarks in the shell.

Second, IEx allows you to break an expression into many lines,
since this is common in Elixir. For example:

    iex(1)> "ab
    ...(1)> c"
    "ab\nc"

In the example above, the shell will be expecting more input until it
finds the closing quote. Sometimes it is not obvious which character
the shell is expecting, and the user may find themselves trapped in
the state of incomplete expression with no ability to terminate it other
than by exiting the shell.

For such cases, there is a special break-trigger (`#iex:break`) that when
encountered on a line by itself will force the shell to break out of any
pending expression and return to its normal state:

    iex(1)> ["ab
    ...(1)> c"
    ...(1)> "
    ...(1)> ]
    ...(1)> #iex:break
    ** (TokenMissingError) iex:1: incomplete expression

hd callback_docsjhd 	type_docsjjAtom  �   `
Elixir.IEx__info__macros	functionserlangget_module_infoinspect_optsElixir.IEx.Configstartconfiguration	start_iexiexElixir.Applicationensure_all_startedok	configurestarted?	MACRO-pryspawn
parse_filefilebadkeyerrorElixir.Filestream!line-maxElixir.EnumsliceinterspersepryenvbindingselfElixir.KernelinspectElixir.String.Chars	to_stringElixir.Pathrelative_to_cwdbit_sizeallregular?nilfalse	byte_sizeElixir.IEx.Server	take_overno_iexstdio	Elixir.IOputsostypewin32after_spawnrun_after_spawnlistsreversereduceensure_module_exists	is_loadedcoderpccallget_object_codeload_binarymodulewidthcolortrueElixir.IO.ANSIformat_fragmentresetiolist_to_binarydont_display_result!do not show this result in outputset_expand_funElixir.Processgroup_leadernodeElixir.IEx.RemshexpandElixir.IEx.Autocompletemake_fun
expand_funiosetoptsmodule_info-run_after_spawn/0-fun-0--start/2-fun-0-initnotify_when_startedstartedwait_until_started Code  =          �   g   � " 0U;U@25BE0@G @@GP@@� N  `�r p� N ��0��@G  ��@� ��PN  ��`� �  @��p09�:� B +��@� ��pH�
��N@��
 ��N P��
 � EEG0EG@F0GGPGG�0� @#@@#g ��N`��
 @�� 
@=�@ F0G
G
G��p@
� p ��@�� 
@=�@ F0G
G
G�p@
@� p �}�1@� �@@Q#@�  �0� EG`@Gp� � �
 0 P�0F 3G
!GE3G�3EG�33F CG
"GEC3D	�@$@#4��5!="!��"@�$$�#$ 
@=%#�$$@ F0G
G
G$�p$@
@$� p %�5&='&��'@�)$�($ 
@=*(�)$@ F0G
G
G$�p)@
@$� p *5+=,+��,�@	(| #o#| #o#� � \@ Z
+� \@Z
+� @�.$�-$ 
=/-�.$@ F0G
G
G$�p.@
@$� p /�;1@
-0
.00@G�=21@$$�2@$53@=43@��4@55$@$=65@$$��6�@| #o#| #o#o�m  \�PZ
+� Z
+� @4#@@D$4D�0@D9=:= B B#+=
0=#;#=@
#7
287@G�@
3� ==8� 99:9 B +9
8@G�=:9@G�:@45;@=<;@��<�@| #o#| 4#o#o	%m  \�Z
+� \Z
+� 44@
3� =@DP>�
9 ?�N @�
: A � ?7B�=CB�C@g@@#@� �0@� D�
> E0  @$@E$3@
?#@
@� @;G@
-F
.FF@$$�!9I:I0`B BB $ECE$CCEC3@C@
@@
D#@@C�"@9H:H B +H
E0G@
-0H� HI�!HJ�#
F K�$N L�0� M@G @ N�%
9OMN�&NP�'
G Q  @�(+R
-@ R@
H�)  @�) !@EE�)" S��
T@� U�*
M V@
NW�+
O X �, #�-
$	%#@*Y#@@
S�. E	%�/&=ZY@
T@#@
U�00'ZPF #G
WGE#@�1 (@G�@�2 ([� 
Z \@� N)]� 
Z^@@� N  _�
[ ` �K @
- a��
\ b  	�#@@#@�3*,c
_�4 +c�5 �+f�6 X+e�7 A+d�@@�8 , d�7He�6Hf�5H   StrT   9 at :Request to pry Cannot pry . Is an IEx shell running?   ImpT      -                         
                                                                                                     #       $   %      &   '      (   )         *         ,         /      0   1      4   5      6   7          9       ;   <         <         =      A   B      @   C         F          9         G      I   J      I   K          L      P   Q          R         R       S   T         V      X   Y               ]   ^      ]   `       0   	   ExpT   �      Z      ^   Z       \   M       V         T   G      Q   9      O   	       M   F       K   9       ?              	                                     
          	      	                   FunT   4      \      b       �	T   [      `       �	TLitT  �  �x�mR�N�0u(��R$X<��B%``��:�K��ؑ�(͜/䏰/-��ӻw�wg3��cW���Ay ٨v��U1�E�y"gPt��КA��d�9ʭ��n�[S�U� (��8�r8�ϥ5AH�k�p��2��<[�SՈ���F�����'�Q>ld�������ˑdӥVk������T4�$=b��&D����PF*��~�p���G<��^�De��Gg���Q]�J&�g2�N�>%|���e��4�j��ۼ���)m
ע�P�pZ�~��	�x#��x��D�KԚ��Q��y!�#.xg�{�޶���O�A�<XN/@��
%��ݵ�4/4�������!��$;�AzA4�Ms�n���o�x�> LocT   X      \      b   [      `   O       X   >      E   :       A                   Attr   (�l   hd vsnl   n D��'�#r1�"Ab�*��jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0.1hd timehb  �aaaa0ahd sourcek N/tmp/vagrant/elixir-build.20160602114703.27319/elixir-1.2.5/lib/iex/lib/iex.exjAbst  �P  [�x��MoEv<_�6�,����F�� �Y� Q�Jђ F�=��jO�xz��=����`�E�? ��XbY2���p�!	ȅHDq�VU�{����ck�RO}��z�����V��!�\3̕ ��vh\|کT*Ot�Ve�C�^�̬X�����*���aCϰ����)�R��q��ʬc����<[7��q;g��%�^����K��(�.��w9�3�N�Y~�����0�����a�n�3��{���7�����E�i�=��鏴=�c�}3��x�L�Ƣ�[��s��Y0t�h�ö��7���&(�I4Ah����yJy�����z1�f[a׬�)D]̲�u=��q����U�:=��ۡ>�1t9���n�������)� �'�#���3����*ln�Ͷ���Z�:�I[m�]����0-�� `A���	�]�C��Bh�Á�JA����+�]I��F�`Ӵ�l�������O+*2���l;�0��8b1bEUJ��	��0�ZBl4�
f��Ht$����^��Ty(�
F����U�wn2O�w^(�w��G�;������^m�����5�H��}� =�>_D�v�c�B��丫�P���}j4͵��/fׅ#��%�>�{"��<2�1�U�vh��F���cӉT�;��+��-NGf��Q�p��Vh�~���.#d��ti���R�]�R�����׏��4E�U�;��� Ȭ �R+�q����b�dQ���:��|�ǈ)��N\
���+M�we2�7�f����WRջ�H]��*SF��=��{0����Ӄ�c��0�2�>�܀G��3b����g��)#��L{)9<�1��-[d�QA<GGT�4�G n���AvM1�2E�"yi���M����"�X�񤇌�Μ~啥e�>�}��L��(kdC���|�x��4�sM�6yn�㡜�:��^.�/���a"x)���lz�v-��lj�S,
��a,��+���0=�S�3p�ᄴ���m�	M�����6��^�_��eaH�#U"��1':�[;JI`@Q�eDA#� j�蹆���Y�j��(�z���0ֺ̅��<`��kb�ΜN0 /�ս�t�\Y��O#�5/x]�BK���GH�zlʹCc�vOz�V6�z_iANE� ��Z���|�|mUxR���k�t�o
�}�}#*`C�P��0,�@������`�E��7ȴ+�!ڍl���ӧ�AL?`����a�S�(��s/&�%.��C?�6��.t�R�ER؅ �;��w�<_Q�	�/�/H�V+Kr�\$<%�?�yWcD�n��|�­UTM�!�%2�k��fO���D4�VR\&�9�0S���)�X�X�v({;T.�4�i�&���$A��e�!�L��@ʠ��Î5���\P.�sd���(�/��=�_�a�4(+�u�m�a��~�g�<�������RŮ\��i�z��y��}���}p��nc�Vn�ĥ��6���N�7�6���j<3�Vs�Ʈ�6vi�Ql��������s�&�W���'lh@?s��J�L�
�0�`���wC����&����i0���F�<��M�r�R���DZ�c�0{r���jc�������:�N�bR��۩u&t��3��f�o'^�FU�&LP�Q�����eK�.��h�m���݋10�b~$��u. DH������c���s�BZ�2�	~S%tS��`���l�v�w4Y�ʂ&=\��h!n!</Ŀ0�eX��ry�:��R�y3w_�1͋n�H�K�{�ѓ��-����(�����HE��I�QF��xl>o��1��7�X�3�gD�'o/[Gr���R&=zM`7l��>��D��bϦJ�Ϝ�I{����Bo�C
�Y�Boj�o�"�>�o��N(�J���Sh,�r��D"�O�Лz
�I���f1Y�\
S@r���C�;�@G����D#���t'�7�M=�3��_s(���H����0Qg���P��KE�f1Y�t(Z 9ߏH��@G��|��8�H��E��"z��:�]>�bA;}���� [��-r������r����1�'�7�ڇ��U��0?Z�%��'ʾ��ɸ��o8 aY�3�x&��
Җ���Tl�������sjo�,.�n���qՌ�w�[�ɇ�Sϟ� ��k;{��SĝԭK�A(z�=p0E��^v��mu�6��'���k#N�l��b^`��{RKO��۩��ېm��9���>dA�
����(]����v�$.�O0ڎl�A-?���*훨$�
t�ݎ̆����x�I�c�FM��롿z{$��i�D%y���Dqz�I? ���L�}u��I������r���2M�[>�d�[�Of�o*����J�$���$j�
d�À�7-��Bw�Q��m��j���2j�]�
��qZv(���9���	��z�o$�:釪:?T����2�e$�� [Ճ�zي>�%���p�f��>���Q��4��.^n���6�a��Y�o�kyk�eC��h��Y"��������+v���K��c���1���:?V�8%���y��Ċ^*�A*�)c�;%�Z��L��\;H�sg2�.����g��ُll'��4����/훨$�r�h��v
�_�x�l���A�t���?t]��by�t%~5?*�9���Tg�]�'��>�\�ނ,��`V�$/ ���·��~[!�e;/ �y!wJ|�DK�������Ug�%�U� u����:�hQ��y�~���&b���yLz��_�Ü�3�t�����M�`*�X�>���H�MR'��c�E�9�x���e�Q�r��	�si2��l�����x�״+\4�|Q3ZO�`���2�b�)����莯?`�B�䧜՚�OM&��	88��G���#��!'��ԥ�����ɶ[m��G�&<�b>9�A��ϔNe�պf/uK;o�VQE'���Ѱ�� ((Ɩ���bឨ��6$�gV���5���PѴ�7u��s���f�@1y����V�~M��Ԙ X��>�,�z)�=%`�ڍ	�&b�Yl��<m�5�9a�q��c��XC�����`܄X3��,�a�&q��Wb4 H��2��N�������l���8�|3���J$ߤɷP"��8��[	��/�Ij��}E�?��0�z>��mO��["#��� ��SA	G	q()�."|���y
AC����$x.�ΐ��9�&�	�r�Н�7����1z�0u�{�{~~�'$k���h#�x��3�r� ��������~���n`�^,#f�GӲU9l{"=��:�ʣx4-�͟>��Y��P��g5:��*��ʗS�`�Svtڥ#ˊ 'G�ث�0�x9�
.�I��i�]�h ����}$��<���o�n�������I�k�2f���Za�Z�����a8���+0h�?(ץd����borZV�ԛH�7I���{m�:�����,���-�-�ժg^*�[���+����D�*;{���1?r�3I�W������1p��v��G�p;f��t�uÁg:T�?!�?���g�/NIė�� ��������a!C`��1���'��J�wTY��#`�`�NΆY�wT���V]f��?����z�x2 �n*!]_�p)}�MlE���S��I�Bu"�w1)�{��״���Q���l�!8`����'��U ��J��J>�o<��R��hYp�(s�v�F�[N][�2{0/��t�   Line   �           d   8   )b)c)�)/)0)�)�)()))D)E)�)�)�)�)�)�)�)�)�)�)�)�)�)�)�)=)>I
IIIII)[)\)6)7)M)N)R)�)�)�)�)�)�)�)�I )�)�)�)�)�)� 
lib/iex.ex   