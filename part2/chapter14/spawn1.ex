defmodule Spawn1 do
  def greet do
    receive do
      {sender, msg} ->
        # 自分自身のPIDをもらっているので、そのまま送り返す
        send sender, {:ok, "Hello, #{msg}, #{:erlang.pid_to_list(sender)}" }
    end
  end
end

# here's a client
pid = spawn(Spawn1, :greet, [])
IO.puts :erlang.pid_to_list(self)

# pidに対して、自分自身のPIDを与えている
send pid, {self, "World!"}

receive do
  {:ok, message} ->
    IO.puts message
end
